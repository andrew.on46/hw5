const button = document.querySelector("button");
const hoverClass = document.querySelectorAll(".hover");
const logoText = document.querySelector(".logo-text");
const mail = document.querySelector(".email");
const tel = document.querySelector(".tel");
const titles = document.querySelectorAll(".images-title");

function colorFunc() {
  if (button.style.backgroundColor === "var(--secondary-color)") {
    button.style.backgroundColor = "var(--primary-color)";
  } else {
    button.style.backgroundColor = "var(--secondary-color)";
  }
  hoverClass.forEach((img) => {
    if (button.style.backgroundColor === "var(--secondary-color)") {
      img.style.backgroundColor = "var(--secondary-color)";
    } else {
      img.style.backgroundColor = "var(--primary-color)";
    }
  });
}

function logoFunc() {
  if (button.style.backgroundColor === "var(--secondary-color)") {
    logoText.style.color = "var(--secondary-color)";
  } else {
    logoText.style.color = "var(--primary-color)";
  }
}

function mailFunc() {
  if (button.style.backgroundColor === "var(--secondary-color)") {
    mail.style.color = "var(--secondary-color)";
  } else {
    mail.style.color = "var(--primary-color)";
  }
}
function telFunc() {
  if (button.style.backgroundColor === "var(--secondary-color)") {
    tel.style.color = "var(--secondary-color)";
  } else {
    tel.style.color = "var(--primary-color)";
  }
}

function titleFunc() {
  titles.forEach((e) => {
    if (button.style.backgroundColor === "var(--secondary-color)") {
      e.style.color = "var(--secondary-color)";
    } else {
      e.style.color = "var(--primary-color)";
    }
  });
}

logoText.addEventListener("mouseleave", () => {
  logoText.style.color = "white";
});

mail.addEventListener("mouseleave", () => {
  mail.style.color = "white";
});

tel.addEventListener("mouseleave", () => {
  tel.style.color = "white";
});

titles.forEach((e) => {
  e.addEventListener("mouseleave", () => {
    e.style.color = "white";
  });
  e.style.color = "white";
});
